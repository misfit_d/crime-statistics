import java.io.*;
import java.util.*;

//Title: USCrimeClassTest.java
//Developed By: Devin Norwood
//Date: 14 October 2018
//Purpose: This application provides the methods to be used with USCrimeClassTest.java to provide the user with a menu that provides 
//them various statistics from the linked .csv file

public class USCrimeClass2 {
//Array for storing data in the .csv file
static String[][] myArray;
	
	//default constructor
	public USCrimeClass2(){
		
	}
	
	
	//method to store the array
	public void storeArray() {
		//initializing my array for number of rows/columns in csv file
		myArray = new String[21][21];
		Scanner scanIn = null;
		int rowC=0;
		String inputLine = "";
		
		String fileLocation;
		
		fileLocation = "C:\\Users\\Devin\\eclipse-workspace\\Final Project\\src\\Crime.csv";
		
		
		try {
			//scanner to read the file variable
			scanIn = new Scanner(new BufferedReader(new FileReader(fileLocation)));
			
			//reads the lines of the .csv file
			while(scanIn.hasNextLine()) {
				inputLine = scanIn.nextLine();
				//temporary array to read the .csv file
				String[] inArray = inputLine.split(",");
				
				//for loop stores the information in a temporary array and passes that to main array
				for(int x=0; x<inArray.length; x++) {
					myArray[rowC][x]= inArray[x];
				}
				rowC++;
			}
				scanIn.close();	
		} catch(Exception e) {
			System.out.println(e);
		}
		
		
		
	}
	
	
	

	//menu method for main application
	public void menu() {
		System.out.println("Please select a menu item:\n\n");
		
		System.out.println("1. View Population Growth");
		System.out.println("2. View year with highest murder rate");
		System.out.println("3. View year with lowest murder rate");
		System.out.println("4. View year of highest robbery rate");
		System.out.println("5. View year of lowest robber rate");
		System.out.println("Press \"0\" to quit");
		System.out.println();
		System.out.println("Please select:");
			}
	
	public void menuSwitch(int option) {
		
		switch (option) {
		case 1:
			getPopGrowth();
		break;
		case 2:
			System.out.println(murderRateHighest() + " had the highest murder rate.");
		break;
		case 3:
			System.out.println(murderRateLowest() + " had the lowest murder rate.");
		break;
		case 4:
			System.out.println(robberyRateHighest() + " had the highest robbery rate.");
		break;
		case 5:
			System.out.println(robberyRateLowest() + " had the lowest robbery rate.");
		break;
		case 0:
			System.exit(0);
		break;
		default:
			System.out.println("Please enter a valid selection from the menu.");}
		} 	
		
		
	
	
	//method for calculating population growth
	public double getPopGrowth() {
		
	double pop = 0;

		for(int x=1;x<myArray.length-1;x++) {
		
		pop = ((Double.parseDouble(myArray[x+1][1])-Double.parseDouble(myArray[x][1]))/Double.parseDouble(myArray[x][1]))*100;
		
		System.out.println(myArray[x][0]+"-"+myArray[x+1][0]);
			System.out.format("%.2f",pop);
			System.out.println("%");
			System.out.println();
			
			
			
		}
		return pop;
	}
	
	//methods for measuring years with the associated statistics 
	public String murderRateHighest() {
		//double to store highest amount
		double murder=0;
		//stores value of year
		String year = "";
		//loop through each row, get murderrate in col 4
		for(int x = 1;x<myArray.length;x++) {
			
			
			double rowValue = Double.parseDouble(myArray[x][5]);
			if(rowValue>murder) {
				murder = rowValue;
				year = myArray[x][0];
			}
					}
		return year;		
		
	}
	
	public String murderRateLowest() {
		//double to store lowest amount
		double murder = 20;
		//String to store the murder's year
		String year = "";
		
		//sort array
		
		
		//for loop to iterate through array
		for(int x=1;x<myArray.length;x++) {
			double rowValue=Double.parseDouble(myArray[x][5]);
			if(rowValue<murder) {
				murder=rowValue;
				year = myArray[x][0];
			}
				
			}
		return year;
		}
	
	
	public String robberyRateHighest() {
		double robbery = 0;
		String year = "";
		
		for(int x = 1;x<myArray.length;x++) {
			double rowValue=Double.parseDouble(myArray[x][9]);
			if(rowValue>robbery) {
				robbery = rowValue;
				year=myArray[x][0];
			}
		}
	return year;	
	}
	
	
	public String robberyRateLowest() {
		//double to store lowest amount
		double robbery = 2000;
		//String to store the murder's year
		String year = "";
		
		//sort array
		
		
		//for loop to iterate through array
		for(int x=1;x<myArray.length;x++) {
			double rowValue=Double.parseDouble(myArray[x][5]);
			if(rowValue<robbery) {
				robbery = rowValue;
				year = myArray[x][0];
			}
				
			}
		return year;
		}
	
	
	
	
	}
	
	
	
	
	
	
	
	
