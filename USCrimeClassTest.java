import java.io.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

//Title: USCrimeClassTest.java
//Developed By: Devin Norwood
//Date: 14 October 2018
//Purpose: This application is meant to be used with USCrimeClass2.java to provide the user with a menu that provides 
//them various statistics from the linked .csv file


public class USCrimeClassTest  {

	public static void main(String[] args) throws IOException {
	//integer to make menu repeat
		int x=1;
		
	
		
//creating instance of class and scanner object		
USCrimeClass2 data = new USCrimeClass2();
Scanner mo = new Scanner(System.in);

//loads the data array from USCrimeClass
data.storeArray();

//while loop to continue displaying menu until "0" is entered
try {	while(x==1) {
	LocalDateTime oldTime = LocalDateTime.now();
	data.menu();
	data.menuSwitch(mo.nextInt());
if(mo.nextInt()==0) {
	LocalDateTime newTime = LocalDateTime.now();
	Duration duration = Duration.between(oldTime, newTime);
	System.out.println("Thank you for using the crime statistics application!");
	System.out.println(duration.getSeconds() + " seconds elapsed.");
	x=x-1;
	mo.close();
	}}

	} catch (Exception e) {
		System.out.println("Invalid input. Please try again.");
	}


}}
